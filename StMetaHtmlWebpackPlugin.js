// StMetaHtmlWebpackPlugin.js
// name: Yime Benites Puelles
// email: benites_alf@hotmail.com
// url:  ......

var request = require('request');
function StMetaHtmlWebpackPlugin(options) {
  // Configure your plugin with options...
  options = options || {};
  this.st_pages = options.st_pages;
}

StMetaHtmlWebpackPlugin.prototype.apply = function(compiler) {
  var self = this;
  // ...
  compiler.plugin('compilation', function(compilation) {
    console.log('The compiler is starting a new compilation...');

    compilation.plugin('html-webpack-plugin-before-html-generation', function(htmlPluginData, callback) {
      if (self.st_pages && htmlPluginData.plugin.options.key_page) {
        var hash_js=htmlPluginData.assets.js[0].split("?")[1];
        htmlPluginData.assets.css=['css/components2.css?'+hash_js,'css/components.css?'+hash_js,'css/app2.css?'+hash_js,'css/app.css?'+hash_js];
        htmlPluginData.assets.js.push("//nexus.ensighten.com/sph/stmicrosite/Bootstrap.js");
        request(self.st_pages[htmlPluginData.plugin.options.key_page].key, function(error, response, json_doc) {
          console.log(error);
          if (!error) {
            // console.log(json_doc);
            var json_doc = JSON.parse(json_doc);
            if(json_doc.lang){
              htmlPluginData.plugin.options.lang=json_doc.lang;
            }
            if(json_doc.title){
              htmlPluginData.plugin.options.title=json_doc.title;
            }
            if(json_doc.meta){
              htmlPluginData.plugin.options.meta=json_doc.meta;
            }

            callback(null,htmlPluginData);
          } else {
            // self.emit('error', new PluginError(StMetaHtmlWebpackPlugin, 'Request to url failed.'));
          }
        });
      }else{
        htmlPluginData.assets.css=['css/components.css','css/app2.css','css/app.css'];
        callback(null,htmlPluginData);
      }
    });


  });


};


module.exports = StMetaHtmlWebpackPlugin;
