"use strict";

var gulp = require('gulp'),
  gutil = require('gulp-util'),
  webpack = require('webpack'),
  gulpWebpack = require('webpack-stream'),
  ExtractTextPlugin = require('extract-text-webpack-plugin'),
  // streamify = require('gulp-streamify'),
  // browserify = require('browserify'),
  compass = require('gulp-compass'),
  connect = require('gulp-connect'),
  open = require("gulp-open"),
  gulpif = require('gulp-if'),
  uglyfly = require('gulp-uglyfly'),
  path = require('path'),
  ftp = require('vinyl-ftp'),
  lint = require('gulp-eslint'),
  glob = require("glob"),
  // PurifyCSSPlugin = require('purifycss-webpack'),
  infoserver = require('../infoserver.json');
var concatFiles = require('gulp-concat-multi');
var plumber = require('gulp-plumber');

var HtmlWebpackPlugin = require('html-webpack-plugin');
var StMetaHtmlWebpackPlugin = require("./StMetaHtmlWebpackPlugin");

var env,
  jsSources,
  sassSources,
  htmlSources,
  outputDir,
  sassStyle,
  jsSourcesST,
  jsSourcesReuters,
  jsLibrary,
  jsLibraryReuters,
  projectSource,
  objEntry;

// values for ENV development,test,production
env = 'test';
// values for projectSource st or reuters
projectSource = 'st';

var env2 = env;
env = (env === "test" || env === "production")
  ? "production"
  : "development";

/*Start define all html, scss and js files*/
// jsSources = ['./scripts/global.js'];
jsSourcesST = ['./scripts/functions.js', './scripts/app.js'];

// only for reuters files
jsSourcesReuters = ["app/scripts/main.js", "app/scripts/functions.js"];
jsLibraryReuters = ["app/scripts/vendor/vendor-top.js", "app/scripts/vendor.js"];

// only for sliders template
jsSourcesReuters = ["app/scripts/slides.js", "app/scripts/functions.js"];
jsLibraryReuters = ["app/scripts/plugins.js"];

jsLibrary = [
  'zepto',
  'd3-selection',
  'd3-transition',
  'd3-dispatch',
  'd3-request',
  'd3-timer',
  'd3-queue',
  'd3-array',
  'iscroll'
];
sassSources = ['app/styles/scss/app.scss', 'app/styles/scss/components.scss'];
// htmlSources = [outputDir + '*.html'];
/*End define all html, scss and js files*/

/*declare parameters for page*/
var server_publish = "http://st-visuals.com/graphics/st-get-file-json/";
if(env2==="production")server_publish="http://graphics.straitstimes.com/st-get-file-json/";
/*declare parameters for page*/
var json_pages_html = {
  "chinaparty": {
    "key": server_publish + "1XQg9jjhr3Bn6j_lBP9SMUNWeMhibvsxArU_aab8irtE",
    "page": "index"
  }
};

/*end setup pages*/

var optionsImg = "";
var optionsImgHtml = "";
var optionsHtml = "";

var optionsFonts;
if (env2 === "development" || env2 === "test") {
  optionsFonts = "limit=1&name=[path][name].[ext]&publicPath=../../../fonts/&context=../fonts/";
} else {
  optionsFonts = "limit=1&name=[path][name].[ext]&publicPath=http://graphics.straitstimes.com/STI/STIMEDIA/Interactives/commons/fonts/&context=../fonts/";
}

var extractCSS = new ExtractTextPlugin({filename: "[name].css", allChunks: true});
var extractSCSS = new ExtractTextPlugin({filename: "[name].css", allChunks: true});
var extractCSSVUE = new ExtractTextPlugin("css/components2.css");

var optionsPluginsJS = [];
var optionsPluginsCSS = [extractCSS, extractSCSS];

if (projectSource !== 'reuters') {
  // jsSources = jsSources.concat(jsSourcesST);
  jsSources = jsSourcesST;
  objEntry = {
    'js/app': jsSources,
    'js/videojs':['video.js'],
    'js/components': jsLibrary,
    'js/scrollmagic': ['ScrollMagic', 'animation.gsap'],
    'js/vuecomponents': [
      'vue-touch','vue-router', 'vue-meta', 'vuex'
    ],
    'js/vue': ['vue'],
    'js/polyfill':['babel-polyfill','iphone-inline-video']
  }
  optionsPluginsJS.push(new webpack.optimize.CommonsChunkPlugin({
    names: ['js/videojs','js/components','js/scrollmagic','js/vuecomponents', 'js/vue','js/polyfill']
  }));

  optionsPluginsJS.push(new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: (env === "development")
        ? '"development"'
        : '"production"',
      PAGES: JSON.stringify(json_pages_html)
    }
  }));

  /*code generation pages*/
  Object.keys(json_pages_html).map(function(key) {
    var generate_html = new HtmlWebpackPlugin({
      title: 'Custom template addaaasd',
      meta: [
        {
          name: 'name1 test',
          content: 'content1 test'
        }, {
          property: 'prop2 test',
          content: 'content2 test'
        }
      ],
      key_page: key,
      filename: ((json_pages_html[key].page)
        ? json_pages_html[key].page
        : key) + '.html',
      template: 'index.ejs',
      hash: true,
      cache: false,
      minify: (env !== 'development')
        ? {
          removeAttributeQuotes: true,
          collapseWhitespace: true,
          html5: true,
          minifyCSS: true,
          removeComments: true,
          removeEmptyAttributes: true
        }
        : false
    });
    optionsPluginsJS.push(generate_html);
  });
  var exec_pages = new StMetaHtmlWebpackPlugin({st_pages: json_pages_html});
  optionsPluginsJS.push(exec_pages);
  /*end generation pages*/

} else {
  objEntry = {
    'js/app2': jsSources
  }
}

if (env !== "development") {
  outputDir = 'production/';
  sassStyle = 'compressed';

  optionsImg = "limit=1000&name=[path][name].[ext]&publicPath=../&outputPath=";
  optionsImgHtml = "limit=10&name=[path][name].[ext]&publicPath=&outputPath=";

  optionsPluginsJS.push(new webpack.optimize.UglifyJsPlugin());
} else {
  outputDir = 'dev/';
  sassStyle = 'expanded';

  optionsHtml = "&emitFile=false";
  // optionsImg = "emitFile=false&limit=1&name=[path][name].[ext]&publicPath=../&outputPath=";
  // optionsImgHtml = "emitFile=false&limit=1&name=[path][name].[ext]&publicPath=&outputPath=";

  optionsImg = "limit=1000&name=[path][name].[ext]&publicPath=../&outputPath=";
  optionsImgHtml = "limit=10&name=[path][name].[ext]&publicPath=&outputPath=";
}
optionsPluginsJS.push(extractCSSVUE);

gulp.task("bundle", function() {
  concatFiles({"js/components.js": jsLibraryReuters, "js/app.js": jsSourcesReuters}).pipe(gulpif(env2 !== 'development', uglyfly())).pipe(gulp.dest(outputDir));

});

gulp.task("js", function() {
  return gulp.src([]).pipe(plumber()).pipe(gulpWebpack({
    context: __dirname + '/app',
    entry: objEntry,
    output: {
      filename: '[name].js'
    },
    resolve: {
      extensions: [
        ".js", '.css', '.png', 'jpg'
      ],
      alias: {
        'vue$': 'vue/dist/vue.esm.js',
        'images': path.resolve(__dirname, 'app/images'),
        'images_doc': path.resolve(__dirname, 'app/images_doc'),
        'artefacts': path.resolve(__dirname, 'app/template/artefacts'),
        'GetSvg': path.resolve(__dirname, 'app/scripts/libraries/get-svg.js'),
        'fixSvgSize': path.resolve(__dirname, 'app/scripts/libraries/fix-svg-size.js'),
        'animateJs': path.resolve(__dirname, 'app/scripts/libraries/animateJs.js'),
        'iphone-inline-video': path.resolve(__dirname, 'app/scripts/libraries/iphone-inline-video'),
        "TweenLite": path.resolve('node_modules', 'gsap/src/uncompressed/TweenLite.js'),
        "TweenMax": path.resolve('node_modules', 'gsap/src/uncompressed/TweenMax.js'),
        "TimelineLite": path.resolve('node_modules', 'gsap/src/uncompressed/TimelineLite.js'),
        "TimelineMax": path.resolve('node_modules', 'gsap/src/uncompressed/TimelineMax.js'),
        "ScrollMagic": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/ScrollMagic.js'),
        "animation.gsap": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js'),
        "debug.addIndicators": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js'),
        modulesProject: path.resolve(__dirname, 'app/scripts/modules.js'),
        "iscroll": path.resolve(__dirname, 'app/scripts/libraries/iscroll-probe.js')
      }
    },
    watch: true,
    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000
    },
    bail: false,
    module: {
      loaders: [
        {
          test: /\.(jpe?g|png|gif|svg|mp4)$/,
          exclude: /(node_modules)/,
          include: [
            path.resolve(__dirname, "app/images"),
            path.resolve(__dirname, "app/images_doc"),
            path.resolve(__dirname, "app/videos")
          ],
          use: [
            'url-loader?' + optionsImgHtml, {
              loader: 'img-loader',
              options: {
                svgo: {
                  plugins: [
                    {
                      convertPathData: false
                    }
                  ]
                }
              }
            }
          ]
        }, {
          test: /\.(mp4)$/,
          exclude: /(node_modules)/,
          include: [path.resolve(__dirname, "app/videos")],
          use: ['url-loader?limit=10&name=[path][name].[ext]&publicPath=&outputPath=']
        }, {
          test: /\.vue$/,
          loader: 'vue-loader',
          options: {
            loaders: {
              scss: extractCSSVUE.extract({
                fallback: 'vue-style-loader',
                use: [
                  {
                    // loader: 'css-loader?url=false&minimize=true'
                    loader: 'css-loader',
                    options: {
                      minimize: (env === "development")
                        ? false
                        : true,
                      root: __dirname + '/app/'
                    }
                  },
                  'postcss-loader',
                  'sass-loader', {
                    loader: 'sass-resources-loader',
                    options: {
                      resources: [path.resolve(__dirname, 'app/styles/scss/_libs.scss')], // for example
                    }
                  }
                ]
              })
            }
          }
        }, {
          test: /\.js$/,
          exclude: /(node_modules)/,
          include: [
            path.resolve(__dirname, "app/scripts"),
            path.resolve(__dirname, "app/template")
          ],
          use: {
            loader: 'babel-loader',
            options: {
              presets: [
                'env', 'react'
              ],
              plugins: ['transform-object-assign', 'transform-runtime']
            }
          }
        }, {
          test: require.resolve('zepto'),
          loader: "imports-loader?this=>window"
        }, {
          test: /\.(html)$/,
          use: [
            {
              loader: 'file-loader?name=[name].html' + optionsHtml
            }, {
              loader: 'extract-loader'
            }, {
              loader: 'html-loader',
              options: {
                minimize: (env === "development")
                  ? false
                  : true
              }
            }
          ]
        }, {
          test: /\.(eot|woff|ttf|woff2|svg)$/,
          exclude: /(node_modules)/,
          include: path.resolve(__dirname, "../fonts"),
          loader: ['url-loader?importLoaders=1&emitFile=false&' + optionsFonts]
        }
      ]
    },
    plugins: optionsPluginsJS
  }, webpack)).pipe(gulp.dest(outputDir));
});

gulp.task("css", function() {
  return gulp.src([]).pipe(plumber()).pipe(gulpWebpack({
    context: __dirname + '/app',
    entry: {
      'css/app2': './styles/precss/app.css',
      'css/app': './styles/scss/app.scss',
      'css/components': './styles/scss/components.scss'
    },
    output: {
      filename: '[name].css'
    },
    resolve: {
      extensions: [
        ".js", '.css'
      ],
      alias: {
        '../images': path.resolve(__dirname, 'app/images'),
        '../images_doc': path.resolve(__dirname, 'app/images_doc')
      }
    },
    watch: true,
    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000
    },
    bail: false,
    module: {
      loaders: [
        {
          test: /\.js$/,
          exclude: /(node_modules)/,
          loader: "babel-loader?presets[]=env&presets[]=react&plugins[]=transform-object-assign"
        }, {
          test: /\.css$/,
          exclude: /(node_modules)/,
          include: path.resolve(__dirname, "app/styles/precss"),
          use: extractCSS.extract({
            fallback: 'style-loader',
            use: [
              {
                // loader: 'css-loader?url=false&minimize=true'
                loader: 'css-loader',
                options: {
                  minimize: (env === "development")
                    ? false
                    : true,
                  root: __dirname + '/app/'
                }
              }, {
                loader: 'postcss-loader'
              }
            ]
          })
        }, {
          test: /\.scss$/,
          exclude: /(node_modules)/,
          include: path.resolve(__dirname, "app/styles/scss"),
          use: extractSCSS.extract({
            fallback: 'style-loader',
            use: [
              {
                // loader: 'css-loader?url=false&minimize=true'
                loader: 'css-loader',
                options: {
                  minimize: (env === "development")
                    ? false
                    : true,
                  root: __dirname + '/app/'
                }
              }, {
                loader: 'postcss-loader'
              }, {
                loader: 'sass-loader'
              }
            ]
          })
        }, {
          test: /\.(eot|woff|ttf|woff2|svg)$/,
          exclude: /(node_modules)/,
          include: path.resolve(__dirname, "../fonts"),
          loader: ['url-loader?emitFile=false&' + optionsFonts]
        }, {
          test: /\.(jpe?g|png|gif|svg)$/,
          exclude: /(node_modules)/,
          include: [
            path.resolve(__dirname, "app/images"),
            path.resolve(__dirname, "app/images_doc")
          ],
          loader: [
            'url-loader?' + optionsImg,
            'img-loader'
          ]
        }
      ]
    },
    plugins: optionsPluginsCSS
  }, webpack)).pipe(gulp.dest(outputDir));

});

gulp.task('compass', function() {
  var path_font = (env2 === "development" || env2 === "test")
    ? "/"
    : "http://graphics.straitstimes.com/STI/STIMEDIA/Interactives/commons/";
  gulp.src(sassSources).pipe(compass({
    sass: 'app/styles/scss',
    css: outputDir + 'css',
    image: outputDir + 'images',
    style: sassStyle,
    // font: outputDir + 'fonts',
    font: 'fonts',
    http_path: path_font,
    relative: false,
    require: ['susy', 'breakpoint']
  }).on('error', gutil.log))
  //    .pipe(gulp.dest( outputDir + 'css'))
    .pipe(connect.reload());
});

gulp.task('lint', function() {
  return gulp.src(outputDir + 'js').pipe(lint({config: 'eslint.config.json'})).pipe(lint.format());
});

gulp.task('watch', function() {
  if (projectSource === 'reuters') {
    gulp.watch(jsSourcesReuters, ['bundle', 'lint']);
  }

  // gulp.watch(jsSources, ['js', 'lint']);
  // gulp.watch([
  //     'development/components/scss/*.scss', 'development/components/scss/*/*.scss'
  // ], ['compass']);
  // gulp.watch('development/*.html', ['html']);
  // gulp.watch('../production/*', ['deploy']);
});

gulp.task('connect', function() {
  connect.server({root: outputDir, livereload: true, port: 8080});
});

gulp.task('open', function() {
  var o = "http://localhost/2017/" + path.parse(__dirname).base + "/" + outputDir;
  gulp.src(outputDir + '/index.html').pipe(open({uri: o}));
});

// gulp.task('html', function() {
//     gulp.src('development/*.html').pipe(gulpif(env === 'production', htmlmin({collapseWhitespace: true, removeComments: true, collapseInlineTagWhitespace: true}))).pipe(gulpif(env === 'production', gulp.dest(outputDir))).pipe(connect.reload());
// });

// gulp.task('svg', function() {
//     gulp.src('development/svg/**/*.svg').pipe(gulpif(env === 'production', htmlmin({collapseWhitespace: true, removeComments: true, collapseInlineTagWhitespace: true}))).pipe(gulpif(env === 'production', gulp.dest(outputDir + 'svg'))).pipe(connect.reload());
// });

// Copy images to production
gulp.task('move', function() {
  // gulp.src('./../fonts/**/*.*')
  //     .pipe(gulpif(env === 'development',gulp.dest(outputDir + 'fonts')));

  // gulp.src('development/images/**/*.*').pipe(gulpif(env === 'production', imagemin())).pipe(gulpif(env === 'production', gulp.dest(outputDir + 'images')));
  gulp.src('app/csv/**/*.*').pipe(gulpif(env === 'production', gulp.dest(outputDir + 'csv')));
  // gulp.src('development/svg/**/*.*')
  //     .pipe(gulpif(env === 'production', gulp.dest(outputDir + 'svg')));
  gulp.src('app/videos/**/*.*').pipe(gulpif(env === 'production', gulp.dest(outputDir + 'videos')));
});

gulp.task('deploy', function() {

  var conn = ftp.create({host: infoserver.servers.development.serverhost, user: infoserver.servers.development.username, password: infoserver.servers.development.password, log: gutil.log});

  var globs = [
    outputDir + '*.html',
    outputDir + 'css/*',
    outputDir + 'csv/*',
    outputDir + 'fonts/**/*',
    outputDir + 'images/**/*',
    outputDir + 'images_doc/**/*',
    outputDir + 'js/*',
    outputDir + 'svg/*',
    outputDir + 'videos/*',
    outputDir + 'babylon/*'
  ];

  return gulp.src(globs, {
    base: outputDir,
    buffer: false
  }).pipe(conn.newerOrDifferentSize('infographics/' + path.parse(__dirname).base)). // only upload newer files
  pipe(conn.dest('infographics/' + path.parse(__dirname).base));

  // .pipe(conn.newerOrDifferentSize('infographics/' + path.parse(path.dirname(path.normalize(__dirname))).base)) // only upload newer files
  // .pipe(conn.dest('infographics/' + path.parse(path.dirname(path.normalize(__dirname))).base));
});

var tasks_to_execute = [
  'css', 'js', 'watch',
  // 'svg',
  'lint',
  'move',
  'open'
];
if (projectSource === 'reuters')
  tasks_to_execute.push('bundle');
gulp.task('default', tasks_to_execute);
