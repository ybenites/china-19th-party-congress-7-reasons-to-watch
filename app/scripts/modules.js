export default {
  "artefacs" : {
    "photo": require("artefacts/photo.vue"),
    subtitle: require("artefacts/subtitle.vue"),
    paragraph: require("artefacts/paragraph.vue"),
    bullets: require("artefacts/bullets.vue"),
    "svg-folder": require("artefacts/st-get-svg.vue"),
    "block-cards": require("artefacts/st-block-cards.vue"),
    "svg-pyramide": require("artefacts/svg-block-pyramide.vue"),
    "media-video": require("artefacts/media-video.vue")
  },
  block : {
    "st-credits": require("../template/main/article/st-credits.vue"),
    "st-next-chapter": require("../template/main/article/next-chapter.vue"),
    "text-intro": require("artefacts/text-intro.vue")
  }
};
