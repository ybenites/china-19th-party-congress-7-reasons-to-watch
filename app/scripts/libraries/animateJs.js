// var d3 = Object.assign({}, require("d3-selection"), require("d3-transition"), require("d3-geo"), require("d3-collection"), require("d3-dispatch"), require("d3-dsv"),require("d3-timer"), require("d3-request"),require("d3-array"));
var d3 = Object.assign({}, require("d3-selection"), require("d3-transition"), require("d3-collection"));
import ScrollMagic from "scrollmagic";
import "scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap";

// import $ from "jquery";
import $ from "zepto";
// require('./waypoint');

export default class Animate {
  constructor(options) {
    if (options) {
      this.options = options;
      var tag = options.tag;
      this.o_type_animations = {
        "animate-up": "slideUp",
        "animate-down": "slideDown",
        "animate-left": "slideLeft",
        "animate-right": "slideRight",
        "animate-fadeIn": "fadeIn",
        "animate-fadeOut": "fadeOut",
        "animate-line": "animatePath",
        "st-AnimateBackgound-virtual": "stAnimateBackgound_virtual",
        "st-AnimateBackgound-health": "stAnimateBackgound_health",
        "st-AnimateBackgound-transport": "stAnimateBackgound_transport",
        "st-AnimateBackgound-environment": "stAnimateBackgound_environment",
        "st-AnimateBackgound-digital": "stAnimateBackgound_digital",
        "st-AnimateBackgound-security": "stAnimateBackgound_security"
      };

      this.entries_animations = d3.entries(this.o_type_animations);

      this.checkIsAnimate(tag);

    }
  }
  checkIsAnimate(tag) {
    var animate = false;
    var newTag = (typeof tag === "object")
      ? d3.select(tag)
      : d3.selectAll(tag);

    if (newTag.size() === 0)
      return;
    this.entries_animations.forEach(d => {
      if (newTag.classed(d.key)) {
        animate = true;
        this.animateTag(newTag, d.value, "in");
      }
    });

    if (!animate) {

      this.entries_animations.forEach(d => {

        if (newTag.selectAll("." + d.key).size() > 0) {
          animate = true;
          this.animateTag(newTag.selectAll("." + d.key), d.value, "in");
        }
      });
    }
    if (!animate) {
      this.animateTag(newTag);
    }
    // return animate;
  }
  animateTag(tags, animateName, type) {
    var _this = this;
    tags.each(function() {
      if (animateName === "animatePath") {
        var elem;
        if ($(this).get(0).tagName === "path")
          elem = $(this).get(0);
        else
          elem = $(this).find("path").get(0);
        var total_length = elem.getTotalLength();

        var d3_elem;
        if ($(this).get(0).tagName === "path")
          d3_elem = d3.select(this);
        else
          d3_elem = d3.select(this).select("path")
        d3_elem.attr('stroke-dasharray', total_length).attr('stroke-dashoffset', total_length);
      }
      if (controller) {
        var element = this;
        var original_element = element;
        var svg = $(element).parents("svg");
        var div_parent = svg.parent().css({"position": "relative", "overflow": "hidden"});
        if (_this.detectIE() && element instanceof SVGElement) {
          // console.log("is IE");

          var offset_svg = svg.offset().left - div_parent.offset().left;
          var size_element = element.getBoundingClientRect();
          var position_element = element.getBBox();
          var new_el = $(`<div style="pointer-events: none;position:absolute;width:${size_element.width + "px"};height:${size_element.height + "px"};top:${ (position_element.y - svg.get(0).viewBox.baseVal.y) + "px"};left:${ (position_element.x + offset_svg - svg.get(0).viewBox.baseVal.x) + "px"} " ></div>`);
          div_parent.append(new_el);
          $(window).on("resize", function() {
            var svg = $(element).parents("svg");
            var div_parent = svg.parent().css("position", "relative");
            var offset_svg = svg.offset().left - div_parent.offset().left;
            var size_element = element.getBoundingClientRect();
            new_el.css({
              "pointer-events": none,
              width: size_element.width + "px",
              height: size_element.height + "px",
              left: (position_element.x + offset_svg - svg.get(0).viewBox.baseVal.x) + "px",
              top: (position_element.y - svg.get(0).viewBox.baseVal.y) + "px"
            });
          });
          element = new_el.get(0);
        }

        var scene = new ScrollMagic.Scene({
          triggerElement: element,
          duration: function() {
            return (this !== undefined)
              ? $(this.triggerElement()).height()
              : 0;
          },
          offset: -0.45 *window.innerHeight
        }).addTo(controller).on("start", function(e) {
          // console.log(e.scrollDirection);
          if (e.scrollDirection === "FORWARD") {
            if (typeof _this.options.todo === "function")
              _this.options.todo(original_element);
            if (animateName !== "animatePath") {
              if (animateName !== undefined)
                $(original_element).animateCss(animateName, type);
              }
            else {
              var d3_elem;
              if ($(original_element).get(0).tagName === "path")
                d3_elem = d3.select(original_element);
              else
                d3_elem = d3.select(original_element).select("path");

              d3_elem.transition().duration(3000).attr('stroke-dashoffset', 0);
            }
          } else {
            if (typeof _this.options.before === "function")
              _this.options.before(element_animated);

            if (animateName !== "animatePath") {} else {
              var d3_elem;
              if ($(original_element).get(0).tagName === "path")
                d3_elem = d3.select(original_element);
              else
                d3_elem = d3.select(original_element).select("path");
              var total_length = d3_elem.node().getTotalLength();
              d3_elem.transition().duration(0).attr('stroke-dasharray', total_length).attr('stroke-dashoffset', total_length);
            }
          }
        });
        // scene.addIndicators();
      }

      // var waypoint_top = new Waypoint({
      //   element: this,
      //   handler: function(direction) {
      //     var element_animated = this.element;
      //     if (direction === "down") {
      //
      //       if (typeof _this.options.todo === "function")
      //         _this.options.todo(element_animated);
      //       if (animateName !== "animatePath") {
      //         if (animateName !== undefined)
      //           $(element_animated).animateCss(animateName, type);
      //         }
      //       else {
      //         var d3_elem;
      //         if ($(element_animated).get(0).tagName === "path")
      //           d3_elem = d3.select(element_animated);
      //         else
      //           d3_elem = d3.select(element_animated).select("path");
      //
      //         d3_elem.transition().duration(3000).attr('stroke-dashoffset', 0);
      //       }
      //     } else if (direction === "up") {
      //       if (typeof _this.options.before === "function")
      //         _this.options.before(element_animated);
      //
      //       if (animateName !== "animatePath") {} else {
      //         var d3_elem;
      //         if ($(element_animated).get(0).tagName === "path")
      //           d3_elem = d3.select(element_animated);
      //         else
      //           d3_elem = d3.select(element_animated).select("path");
      //         var total_length = d3_elem.node().getTotalLength();
      //         d3_elem.transition().duration(0).attr('stroke-dasharray', total_length).attr('stroke-dashoffset', total_length);
      //       }
      //     }
      //
      //   },
      //   offset: "100%"
      // });

    });
  }

  detectIE() {
    var ua = window.navigator.userAgent;

    // Test values; Uncomment to check result …

    // IE 10
    // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

    // IE 11
    // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

    // Edge 12 (Spartan)
    // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

    // Edge 13
    // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf('rv:');
      return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
  }
  animateCss() {}
  animateHand() {}
}
