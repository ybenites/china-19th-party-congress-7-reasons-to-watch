import Vuex from "vuex";

export default class Management {
  constructor(Vue, options) {
    Vue.use(Vuex);

    this.store = new Vuex.Store({
      state: {
        loading: false,
        fullHeight: window.innerHeight,
        fullWidth: window.innerWidth,
        identifier:null,
        menu: [],
        date:false,
        header:false,
        article:false,
        credits:false,
        nextChapter:false,
        textIntro:false,
      },
      getters: {
      },
      mutations: {
        loading(state, value) {
          state.loading = value;
        },
        fullHeight(state,height){
          state.fullHeight=height;
        },
        fullWidth(state,width){
          state.fullWidth=width;
        },
        identifier(state,identifier){
          state.identifier=identifier;
        },
        pushResponse(state,response){
          if (response.menu !== undefined) {
            state.menu=response.menu;
          }
          if(response.date!==undefined){
            state.date=response.date;
          }
          if(response.header!==undefined){
            state.header=response.header;
          }
          if(response.article!==undefined){
            state.article=response.article;
          }
          if(response.credits!==undefined){
            state.credits=response.credits;
          }
          if(response.next_chapter!==undefined){
            state.nextChapter=response.next_chapter;
          }

          if(response.text_intro!==undefined){
            state.textIntro=response.text_intro;
          }
        }
      },
      strict: options.debug
    });

  }
}
